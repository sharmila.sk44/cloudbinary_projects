#### Session Video:
    https://drive.google.com/file/d/1Rs9lxQY3rSrfV3UImBVZDf4Hrh7fhD8i/view?usp=sharing


#### 

1. GitHub
    - iac : terraform code to provision jenkins 
    -  Developer Backend : Java Code. :
        - uploaded to repository : https://github.com/kesavkummari/cloudbinary_awsdevops_projects.git

2. Jenkins :
    - Create a Job in Jenkins :
        - Scripted & Declarative

3. SonarQube. :
    - Setup CSCAT
    - Integrate with Jenkins

4. JFrog

Terraform :
    GitHub Repository :  https://github.com/kesavkummari/cloudbinary_awsdevops_projects.git


mvn clean verify sonar:sonar \
  -Dsonar.projectKey=cloudbinaryio \
  -Dsonar.projectName='cloudbinaryio' \
  -Dsonar.host.url=http://18.206.176.220:9000 \
  -Dsonar.token=sqp_ee60244f58bb57abe4bccfbcf177a686037bf14a

  